$$P\left(A \cap B\right) = P\left(A|B\right) \cdot P\left(B\right)$$

$$P\left(A \cap B\right) = P\left(B|A\right) \cdot P\left(A\right)$$
