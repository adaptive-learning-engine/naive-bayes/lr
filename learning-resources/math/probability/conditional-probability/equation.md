$$P\left(A\middle|B\right)=\frac{P\left(A \cap B\right)}{P\left(B\right)}$$
