$$P\left(A\right)=\frac{P\left(B \cap A\right)}{P\left(B\middle|A\right)}$$

$$P\left(B\right)=\frac{P\left(A \cap B\right)}{P\left(A\middle|B\right)}$$
