$$P\left(A | B\right) = P\left(A\right), \quad P\left(B\right) \neq 0$$

$$P\left(B | A\right) = P\left(B\right), \quad P\left(A\right) \neq 0$$
