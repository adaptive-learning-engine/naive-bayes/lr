$$P\left(A \cap B\right) = P(A) \cdot P(B)$$
